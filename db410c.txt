[host_machine]
system = 'linux'
cpu_family = 'aarch64'
cpu = 'aarch64'
endian = 'little'

[properties]
sys_root = '/srv/nfs/nicolas/db410c'
pkg_config_libdir = '/srv/nfs/nicolas/db410c/usr/lib/pkgconfig:/srv/nfs/nicolas/db410c/usr/lib64/pkgconfig:/srv/nfs/nicolas/db410c/usr/share/pkgconfig'

c_args = ['--sysroot=/srv/nfs/nicolas/db410c']
cpp_args = ['--sysroot=/srv/nfs/nicolas/db410c', '-I/srv/nfs/nicolas/db410c/usr/include/c++/10', '-I/srv/nfs/nicolas/db410c/usr/include/c++/10/aarch64-redhat-linux']
c_link_args = ['--sysroot=/srv/nfs/nicolas/db410c']
cpp_link_args = ['--sysroot=/srv/nfs/nicolas/db410c', '-L/srv/nfs/nicolas/db410c/usr/lib/gcc/aarch64-redhat-linux/10', '-lgcc_s']

[binaries]
c = 'aarch64-linux-gnu-gcc'
cpp = 'aarch64-linux-gnu-g++'
ar = 'aarch64-linux-gnu-ar'
strip = 'aarch64-linux-gnu-strip'
pkg-config = 'pkg-config'
