[constants]
NFSROOT = '/srv/nfs/FIXME'
GCC_VERSION = '13'
ARGS = ['--sysroot=' + NFSROOT]

[host_machine]
system = 'linux'
cpu_family = 'aarch64'
cpu = 'aarch64'
endian = 'little'

[properties]
sys_root = NFSROOT
pkg_config_libdir = NFSROOT / 'usr/lib/pkgconfig:' + NFSROOT / 'usr/lib64/pkgconfig:' + NFSROOT / 'usr/share/pkgconfig'

[built-in options]
c_args =        ['--sysroot=' + NFSROOT]

cpp_args =      ['--sysroot=' + NFSROOT,
                 '-I' + NFSROOT / 'usr/include/c++' / GCC_VERSION,
                 '-I' + NFSROOT / 'usr/include/c++' / GCC_VERSION / 'aarch64-redhat-linux']

c_link_args =   ['--sysroot=' + NFSROOT]

cpp_link_args = ['--sysroot=' + NFSROOT,
                 '-L' + NFSROOT / 'usr/lib/gcc/aarch64-redhat-linux' / GCC_VERSION,
                 '-lgcc_s']

[binaries]
c = 'aarch64-linux-gnu-gcc'
cpp = 'aarch64-linux-gnu-g++'
ar = 'aarch64-linux-gnu-ar'
strip = 'aarch64-linux-gnu-strip'
pkg-config = 'pkgconf'
llvm-config = '/home/nicolas/Sources/meson-cross/llvm-config.wraper'
